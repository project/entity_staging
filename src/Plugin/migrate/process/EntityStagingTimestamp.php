<?php

namespace Drupal\entity_staging\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Converts date string to timestamp.
 *
 * Examples:
 *
 * @code
 * process:
 *   field_date:
 *     plugin: entity_staging_timestamp
 *     source: event_date
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "entity_staging_timestamp"
 * )
 */
class EntityStagingTimestamp extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value) && $value !== '0' && $value !== 0) {
      return '';
    }

    if (((string) (int) $value === (string) $value) && ($value <= PHP_INT_MAX) && ($value >= ~PHP_INT_MAX)) {
      return $value;
    }
    return strtotime($value);
  }

}
