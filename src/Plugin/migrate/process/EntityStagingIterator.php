<?php

namespace Drupal\entity_staging\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\SubProcess;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin iterates and processes an array.
 *
 * @MigrateProcessPlugin(
 *   id = "entity_staging_iterator",
 *   handle_multiples = TRUE
 * )
 */
class EntityStagingIterator extends SubProcess {

  /**
   * Runs a process pipeline on each destination property per list item.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value) && !is_array($value[0])) {
      $new_value = [$value];
    }
    else {
      $new_value = $value;
    }
    return parent::transform($new_value, $migrate_executable, $row, $destination_property);
  }

}
