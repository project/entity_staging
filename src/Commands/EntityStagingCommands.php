<?php

namespace Drupal\entity_staging\Commands;

use Drupal\entity_staging\EntityStagingExport;
use Drupal\entity_staging\EntityStagingImport;
use Drush\Commands\DrushCommands;

/**
 * Define all drush commands for entity staging.
 */
class EntityStagingCommands extends DrushCommands {

  /**
   * Import service.
   *
   * @var \Drupal\entity_staging\EntityStagingImport
   */
  protected $importService;

  /**
   * Export service.
   *
   * @var \Drupal\entity_staging\EntityStagingExport
   */
  protected $exportService;

  /**
   * EntityStagingCommands constructor.
   *
   * @param \Drupal\entity_staging\EntityStagingImport $importService
   *   Import service.
   * @param \Drupal\entity_staging\EntityStagingExport $exportService
   *   Export service.
   */
  public function __construct(EntityStagingImport $importService, EntityStagingExport $exportService) {
    $this->importService = $importService;
    $this->exportService = $exportService;
  }

  /**
   * Export all contents.
   *
   * @command entity_staging:export
   *
   * @param string $contents
   *   Optional comma-separated list of contents to export. Format is entity_type_id:entity_id.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option merge
   *   If merge option is setted, previous exports and the current are merged.
   * @aliases ex, export-content
   */
  public function content($contents = NULL, $options = ['merge' => 0]) {
    if (NULL !== $contents) {
      $contents = explode(',', $contents);
    }
    $this->exportService->export($contents, $options);

    // For compatibilty.
    if (function_exists('_drush_log_drupal_messages')) {
      _drush_log_drupal_messages();
    }
  }

  /**
   * Update migration config according the exported entities.
   *
   * @command entity_staging:update-migration-config
   * @aliases umc, update-migration-config
   */
  public function migrationConfig() {
    $this->importService->createMigrations();

    // For compatibilty.
    if (function_exists('_drush_log_drupal_messages')) {
      _drush_log_drupal_messages();
    }
  }

}
