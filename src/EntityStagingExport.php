<?php

namespace Drupal\entity_staging;

use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_staging\Event\EntityStagingBeforeExportEvent;
use Drupal\entity_staging\Event\EntityStagingEvents;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\path_alias\AliasRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Export content entities.
 */
class EntityStagingExport {

  use StringTranslationTrait;

  /**
   * The content staging manager service.
   *
   * @var \Drupal\entity_staging\EntityStagingManager
   */
  protected $contentStagingManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The alias repository service.
   *
   * @var \Drupal\path_alias\AliasRepositoryInterface
   */
  protected $aliasRepository;

  /**
   * The serializer service.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Should be merged with previous exports ?
   *
   * @var bool
   */
  protected $merge;

  /**
   * EntityStagingExport constructor.
   *
   * @param \Drupal\entity_staging\EntityStagingManager $entity_staging_manager
   *   The content staging manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\path_alias\AliasRepositoryInterface $alias_repository
   *   The alias repository service.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer service.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher interface.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityStagingManager $entity_staging_manager, EntityTypeManagerInterface $entity_type_manager, AliasRepositoryInterface $alias_repository, Serializer $serializer, FileSystem $file_system, EventDispatcherInterface $event_dispatcher, Settings $settings, MessengerInterface $messenger) {
    $this->contentStagingManager = $entity_staging_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->aliasRepository = $alias_repository;
    $this->serializer = $serializer;
    $this->fileSystem = $file_system;
    $this->eventDispatcher = $event_dispatcher;
    $this->settings = $settings;
    $this->messenger = $messenger;
  }

  /**
   * Export content entities.
   */
  public function export(array $contents = NULL, $options = []) {
    $this->merge = (isset($options['merge'])) ? (bool) $options['merge'] : FALSE;

    if (NULL !== $contents) {
      foreach ($contents as $content) {
        list($entity_type_id, $entity_id) = explode(':', $content);
        $content_entities[$entity_type_id][$entity_id] = $entity_id;
      }
    }

    $types = $this->contentStagingManager->getContentEntityTypes(EntityStagingManager::ALLOWED_FOR_STAGING_ONLY);
    foreach ($types as $entity_type_id => $entity_info) {
      $entities = [];
      if (NULL !== $contents) {
        if (isset($content_entities[$entity_type_id])) {
          /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
          foreach ($this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($content_entities[$entity_type_id]) as $entity) {
            $bundle = NULL;
            if ($entity_info->hasKey('bundle')) {
              $bundle = $entity->bundle();
            }
            $entities += [$bundle => []];
            $entities[$bundle] = array_merge_recursive($entities[$bundle], $this->getTranslatedEntity($entity));
          }

          foreach ($entities as $bundle => $entities_list) {
            $this->doExport($entities_list, $entity_type_id, $bundle);
          }
        }
      }
      elseif ($entity_info->hasKey('bundle')) {
        $bundles = $this->contentStagingManager->getBundles($entity_type_id, EntityStagingManager::ALLOWED_FOR_STAGING_ONLY);
        foreach ($bundles as $bundle => $entity_label) {
          $entities = [];
          /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
          foreach ($this->entityTypeManager->getStorage($entity_type_id)->loadByProperties([$entity_info->getKey('bundle') => $bundle]) as $entity) {
            $entities = array_merge_recursive($entities, $this->getTranslatedEntity($entity));
          }

          $this->doExport($entities, $entity_type_id, $bundle);
        }
      }
      else {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        foreach ($this->entityTypeManager->getStorage($entity_type_id)->loadMultiple() as $entity) {
          $entities = array_merge_recursive($entities, $this->getTranslatedEntity($entity));
        }

        $this->doExport($entities, $entity_type_id);
      }
    }
  }

  /**
   * Get all translation for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity to retrieve associated translations.
   *
   * @return \Drupal\Core\Entity\EntityInterface[][][]
   */
  protected function getTranslatedEntity(ContentEntityInterface $entity) {
    $entities = [];
    foreach ($entity->getTranslationLanguages() as $content_language) {
      $translated_entity = $entity->getTranslation($content_language->getId());
      $translated_entity->path = NULL;
      if ($translated_entity->hasLinkTemplate('canonical')) {
        $entity_path = $translated_entity->toUrl('canonical')
          ->getInternalPath();

        $translated_entity->path = $this->aliasRepository->lookupByAlias('/' . $entity_path, $content_language->getId());
      }
      if ($translated_entity->isDefaultTranslation()) {
        $entities['default_language'][$entity->getEntityTypeId()][] = $translated_entity;
      }
      else {
        $entities['translations'][$entity->getEntityTypeId()][] = $translated_entity;
      }
    }

    return $entities;
  }

  /**
   * Proceed the export.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[][][] $translated_entities
   *   All translated entities.
   * @param string $entity_type_id
   *   The entities type id.
   * @param string|null $bundle
   *   The entities bundle.
   *
   * @throws \Exception
   *   An error occured during treatment.
   */
  protected function doExport(array $translated_entities, $entity_type_id, $bundle = NULL) {
    $staging_path = DRUPAL_ROOT . '/' . $this->contentStagingManager->getDirectory();
    // Get system real path.
    $export_path = realpath($staging_path);

    // Check if staging base path exists.
    if (!$export_path) {
      // Attempt to create the staging base path.
      if (!mkdir($staging_path)) {
        throw new \Exception($this->t('Could not create base path directory @path', [
          '@path' => $staging_path,
        ]));
      }
    }

    foreach ($translated_entities as $language => $entities) {
      $event = new EntityStagingBeforeExportEvent($entity_type_id, $bundle, $entities);
      /** @var EntityStagingBeforeExportEvent $event */
      $event = $this->eventDispatcher->dispatch(EntityStagingEvents::BEFORE_EXPORT, $event);
      $entity_list = $event->getEntities();

      $entity_export_path = $export_path . '/' . $entity_type_id . '/' . $language;

      // Ensure the folder exists.
      if (!file_exists($entity_export_path)) {
        // Attempt to create folder.
        if (!mkdir($entity_export_path, $this->settings->get('file_chmod_directory', 0775), TRUE)) {
          throw new \Exception($this->t('Could not create entity @entity path directory @path', [
            '@entity' => $entity_type_id,
            '@path' => $entity_export_path,
          ]));
        }
      }

      $export_file_path = $entity_export_path . '/' . $entity_type_id . '.json';
      if ($bundle) {
        $export_file_path = $entity_export_path . '/' . $bundle . '.json';
      }

      // Recreate array of entities with uuid as key.
      $entity_list_uuid = [$entity_type_id => []];
      foreach ($entity_list[$entity_type_id] as $entity) {
        $entity_list_uuid[$entity_type_id][$entity->uuid()] = $entity;
      }

      if ($this->merge === TRUE && file_exists($export_file_path)) {

        // Get existing entities.
        $input = file_get_contents($export_file_path);
        $entity_list_uuid_existing = json_decode($input, TRUE);

        // Deal with previous json format when no uuid was used as key.
        $existing_entities = current($entity_list_uuid_existing);
        $existing_keys = array_keys($existing_entities);
        if (current($existing_keys) === '0') {
          $entity_list_uuid_existing = [$entity_type_id => []];
          foreach ($existing_entities as $entity) {
            $uuid = $entity['uuid'][0]['value'];
            $entity_list_uuid_existing[$entity_type_id][$uuid] = $entity;
          }
        }
        $entity_list_uuid[$entity_type_id] = array_merge($entity_list_uuid_existing[$entity_type_id], $entity_list_uuid[$entity_type_id]);
      }

      ksort($entity_list_uuid[$entity_type_id]);

      $serialized_entities = $this->serializer->serialize($entity_list_uuid, 'json', [
        'json_encode_options' => JSON_PRETTY_PRINT,
      ]);

      file_put_contents($export_file_path, $serialized_entities);

      $this->messenger->addMessage($this->t('Export @entity_type - @langcode - @bundle entities', [
        '@entity_type' => $entity_type_id,
        '@langcode' => $language,
        '@bundle' => $bundle,
      ]));
    }
  }

}

