#Entity Staging

##Exporting content:


1. After enabling this module, go to /admin/config/system/content-staging:
     - Choose all entity types / bundles you want to export the content
     - Change the default content staging directory ('../staging' by default).
       This directory is relative to the drupal root.

2. Run the drush command: `$ drush export-content (ex)`

3. Don't forget to export the entity_staging configurarion (entity_staging.settings.yml)

### Export specific content

You can choose to export specific entity during step 2. passing entity type and entity id as argument : `drush ex [entity_type_id]:[entity_id]`.

Following example will export the node entity type with entity id = 1:
`drush ex node:1`

In case you want to export multiple entities, separate them by comma : `drush ex [entity_type_id]:[entity_id],[entity_type_id]:[entity_id]`

Following example will export node entity types with entity id 1 and 5 and taxonomy term entity type with entity id 3:
`drush ex node:1,node:5,taxonomy_term:3`

### Merge option

By default, each time you export some entities, existing json file is overwritten by the new export.
If you want to keep existing data in json file and only add and update new ones, add `--merge` option.

Ex:
`drush ex --merge`

##Importing content:

1. Run the drush command to update migration entities
   regarding the previous configuration: `$ drush update-migration-config`

2. Run the migration: `$ drush mi --group=entity_staging (umc)`

